<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Home</title>
<style type="text/css">
h1{
color:#1c1282
}
body{
background-color:#e3bab6
}
</style>
</head>
<body>

<center>
       <h1>HOSPITAL MANAGEMENT SYSTEM</h1>
       <form action="view" method="post" modelAttribute="a">
		<table>

			<tr>
				<td><h3>
						<a href="<c:url value="/enroll"/>">Enroll Patient</a>
					</h3></td>
			</tr>

			<tr>
				<td>
					<h3>
						<a href="<c:url value="/physician"/>">Add Physician</a>
					</h3>
				</td>
			</tr>

			<tr>
				<td>
				<h3>
						<a href="<c:url value="/search"/>">Physician Search</a>
					</h3></td>
			</tr>

			<tr>
				<td><h3>
						<a href="<c:url value="/diagnosis"/>">Patient Diagnosis
							Details</a>
					</h3></td>
			</tr>
			<tr>
			<td><h3>
			<a href="<c:url value="/history/"/>">View Patient History</a>
			</h3></td></tr>
		</table>
		</form>
	</center>

</body>
</html>