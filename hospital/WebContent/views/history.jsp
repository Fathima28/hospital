<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>View Patient History</title>
<style type="text/css">
h1{
color:#1c1282
}
body{
background-color:#e3bab6
}
</style>
</head>
<body>
<h1 align="center">Patient History</h1>
<a href="home">Home</a>
<a href="index">Logout</a>
<center>
<table >

			<tr >
				<th>PATIENT ID</th>
				<th>FIRST NAME</th>
				<th>LAST NAME</th>
			</tr>
			<c:forEach items="${ep}" var="a">
				<tr>
				<td>${a.getPatientId()}</td>
				<td>${a.getFirstName()}</td>
				<td>${a.getLastName()}</td>
				</tr>
			
			</c:forEach>			
		</table>
</center>
</body>
</html>