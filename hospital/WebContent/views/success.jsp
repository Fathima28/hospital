<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Success</title>
<style type="text/css">
h1{
color:#1c1282
}
body{
background-color:#e3bab6
}
</style>
</head>
<body>

		<h1 align="center">Physician Details</h1>
		<a href="home">Home</a>
		<br/>
		<a href="index">Logout</a>
		<br/>
		<center>
		<table border="1">
			<tr>
				<th>Physician Id</th>
				<th>Physician First name</th>
				<th>Physician Last name</th>
				<th>Department</th>
				<th>Educational qualification</th>
				<th>Years of Experience</th>
				<th>State</th>
				<th>Insurance Plan</th>
			</tr>
			<c:forEach items="${physicianList}" var="physician">
				<tr>
					<td>${physician.physicianId}</td>
					<td>${physician.physicianFirstName}</td>
					<td>${physician.physicianLastName}</td>
					<td>${physician.department}</td>
					<td>${physician.educationalQualification}</td>
					<td>${physician.yearsOfExperience}</td>
					<td>${physician.state}</td>
					<td>${physician.insurancePlan}</td>
			</c:forEach>
		</table>
		</center>
</body>
</html>