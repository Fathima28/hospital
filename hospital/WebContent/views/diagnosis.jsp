<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri="http://www.springframework.org/tags/form" prefix="s" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Diagnosis</title>
<style type="text/css">
h1{
color:#1c1282
}
body{
background-color:#e3bab6
}
</style>
</head>
<body>
<h1 align="center">Patient Diagnosis Details</h1>
<hr>
<a href="home">Home</a>
<br>
<a href="index">Logout</a>
<br/>
<br/>
<s:form action="addDiagnosis" method="post" modelAttribute="a" >
<center>
<table>
<tr>
<td>PatientId:</td><td><s:input path="patientId" autocomplete="off"></s:input></td></tr>
<tr><td>DiagnosisId:</td><td><s:input path="diagnosisId" autocomplete="off"></s:input></td></tr>
<tr>
<td>Symptoms</td><td><s:input  path="symptoms" autocomplete="off"></s:input></td></tr>
<tr><td>Diagnosis Provided:</td><td><s:input path="diagnosisProvided" autocomplete="off"></s:input></td></tr>
<tr><td>Administered By:</td><td><s:input  path="administeredBy" ></s:input></td></tr>
<tr><td>Date Of Diagnosis:</td><td><s:input type="date" path="dateOfDiagnosis"></s:input></td></tr>
<tr><td>FollowUp Required:</td><td><s:input  path="followUpRequired" autocomplete="off"></s:input></td></tr>
<tr><td>Date of FollowUp:</td><td><s:input type="date" path="dateOfFollowUp" autocomplete="off"></s:input></td></tr>
<tr><td>Bill Amount:</td><td><s:input path="billAmount" autocomplete="off"></s:input></td></tr>
<tr><td>Card Number:</td><td><s:input path="cardNumber" autocomplete="off"></s:input></td></tr>
<tr><td>Mode Of Payment:</td><td><s:input path="modeOfPayment" autocomplete="off"></s:input></td></tr>

<tr><td><s:button>Add</s:button></td></tr>
</table>
</center>

</s:form>

</body>
</html>