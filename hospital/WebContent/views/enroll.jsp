<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="s" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>enroll</title>
<style type="text/css">
h1{
color:#1c1282

}
body{
background-color:#e3bab6
}

</style>
</head>
<body>
<h1 align="center">Enroll Patient</h1>
<hr>
<a href="home">Home</a>
<br>
<a href="index">Logout</a>
<br/>
<br/>
<s:form action="add" method="post" modelAttribute="a" >
<center>
<table>
<tr>
<td>PatientId:</td><td><s:input path="patientId" autocomplete="off"></s:input></td></tr>
<tr>
<td>PatientFirstName:</td><td><s:input  path="firstName" autocomplete="off"></s:input></td></tr>
<tr><td>PatientLastName:</td><td><s:input path="lastName" autocomplete="off"></s:input></td></tr>
<tr><td>Password:</td><td><s:input type="password" path="password" ></s:input></td></tr>
<tr><td>Date Of Birth:</td><td><s:input type ="date" path="dob"></s:input></td></tr>
<tr><td>Patient EmailId:</td><td><s:input type="email" path="email" autocomplete="off"></s:input></td></tr>
<tr><td>Patient Contact Number:</td><td><s:input path="contact" autocomplete="off"></s:input></td></tr>
<tr><td>State:</td><td><s:input path="state" autocomplete="off"></s:input></td></tr>
<tr><td>Patient Insurance Plan:</td><td><s:input path="insurancePlan" autocomplete="off"></s:input></td></tr>
<tr><td><s:button>Add</s:button></td></tr>
</table>
</center>
</s:form>

</body>
</html>