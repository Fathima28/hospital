<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="s" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style type="text/css">
h1{
color:#1c1282
}
body{
background-color:#e3bab6
}
</style>
</head>
<body>
<h1 align="center">Add Physician</h1>
<hr>
<a href="home">Home</a>
<br><br>
<a href="index">Logout</a>
<br/>
<br/>
<s:form action="addphysician" method="post" modelAttribute="a" >
<center>
<table>
<tr>
<td>PhysicianId:</td><td><s:input path="physicianId" autocomplete="off"></s:input></td></tr>
<tr><td>PhysicianFirstName:</td><td><s:input path="physicianFirstName" autocomplete="off"></s:input></td></tr>
<tr><td>PhysicianLastName:</td><td><s:input path="physicianLastName" autocomplete="off"></s:input></td></tr>
<tr><td>Department:</td><td><s:input  path="department" ></s:input></td></tr>
<tr><td>Educational Qualification:</td><td><s:input  path="educationalQualification"></s:input></td></tr>
<tr><td>Years Of Experience:</td><td><s:input  path="yearsOfExperience" autocomplete="off"></s:input></td></tr>
<tr><td>State:</td><td><s:input path="state" autocomplete="off"></s:input></td></tr>
<tr><td> Insurance Plan:</td><td><s:input path="insurancePlan" autocomplete="off"></s:input></td></tr>
<tr><td><s:button>AddPhysician</s:button></td></tr>
</table>
</center>
</s:form>

</body>
</html>