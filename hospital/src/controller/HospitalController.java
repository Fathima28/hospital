package controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import dao.HospitalDao;
import dao.HospitalDaoImpl;
import model.Admin;
import model.Patient;
import model.PatientDiagnosisDetails;
import model.Physician;


@Controller
public class HospitalController {
	
	HospitalDao dao=new HospitalDaoImpl();
	@RequestMapping("adminLogin")
	public ModelAndView adminLogin(@ModelAttribute("a") Admin admin) {
		boolean isValid=dao.adminLogin(admin);
		if(isValid) {
		return new ModelAndView("home");
	}
		else
		{
			return new ModelAndView("adminLogin","v","Invalid Credentials");
		}
		}
	@RequestMapping("enroll")
	public ModelAndView details()
	{
		return new ModelAndView("enroll","a",new Patient());
		
	}
	@RequestMapping("add")
	public ModelAndView enroll(@ModelAttribute("a") Patient patient)
	{
		dao.enroll(patient);
		return new ModelAndView("display","a",patient);
	}
	@RequestMapping("home")
	public ModelAndView home()
	{
		return new ModelAndView ("home","a",new Patient());
	}
	@RequestMapping("index")
	public ModelAndView logout()
	{
		return new ModelAndView("adminLogin","v","LoggedOutSucessfully!!!");
	}
	
	@RequestMapping("physician")
	public ModelAndView physician(@ModelAttribute("a") Physician physician)
	{
		
		return new ModelAndView("physician","a",physician);
	}
	@RequestMapping("addphysician")
	public ModelAndView addPhysician(@ModelAttribute("a") Physician physician)
	{
		dao.addPhysician(physician);
		return new ModelAndView("display","a",physician);
	}
	@RequestMapping("search")
	public ModelAndView search(@ModelAttribute("a") Physician physician) {
		return new ModelAndView("search","a",physician);		
	}
	@RequestMapping("submit")
	public ModelAndView searchPhysician(@ModelAttribute("a") Physician physician)
	{
		dao.search(physician);
		return new ModelAndView("success","a",physician);
	}
	@RequestMapping("diagnosis")
	public ModelAndView diagnosis(@ModelAttribute("a") PatientDiagnosisDetails diagnosis)
	{
		return new ModelAndView("diagnosis","a",diagnosis);
		
	}
	@RequestMapping("addDiagnosis")
	public ModelAndView adddiagnosis(@ModelAttribute("a") PatientDiagnosisDetails diagnosis)
	{
		dao.diagnosisDetails(diagnosis);
		return new ModelAndView("display","a",diagnosis);
	}
	@RequestMapping("history")
	public ModelAndView history()
	{
		return new ModelAndView("history","a",new Patient());
		
	}
}



