package model;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class PatientDiagnosisDetails {
private Integer patientId;
@Id
private Integer diagnosisId;
private String symptoms;
private String diagnosisProvided;
private String administeredBy;
private Date dateOfDiagnosis;
private String followUpRequired;
private Date dateOfFollowUp ;
private Float billAmount ;
private Integer cardNumber;
private String modeOfPayment;
public Integer getPatientId() {
	return patientId;
}
public void setPatientId(Integer patientId) {
	this.patientId = patientId;
}
public Integer getDiagnosisId() {
	return diagnosisId;
}
public void setDiagnosisId(Integer diagnosisId) {
	this.diagnosisId = diagnosisId;
}

public String getSymptoms() {
	return symptoms;
}
public void setSymptoms(String symptoms) {
	this.symptoms = symptoms;
}
public String getDiagnosisProvided() {
	return diagnosisProvided;
}
public void setDiagnosisProvided(String diagnosisProvided) {
	this.diagnosisProvided = diagnosisProvided;
}
public String getAdministeredBy() {
	return administeredBy;
}
public void setAdministeredBy(String adminsteredBy) {
	this.administeredBy = adminsteredBy;
}
public Date getDateOfDiagnosis() {
	return dateOfDiagnosis;
}
public void setDateOfDiagnosis(Date dateOfDiagnosis) {
	this.dateOfDiagnosis = dateOfDiagnosis;
}
public String getFollowUpRequired() {
	return followUpRequired;
}
public void setFollowUpRequired(String followUpRequired) {
	this.followUpRequired = followUpRequired;
}
public Date getDateOfFollowUp() {
	return dateOfFollowUp;
}
public void setDateOfFollowUp(Date dateOfFollowUp) {
	this.dateOfFollowUp = dateOfFollowUp;
}
public Float getBillAmount() {
	return billAmount;
}
public void setBillAmount(Float billAmount) {
	this.billAmount = billAmount;
}
public Integer getCardNumber() {
	return cardNumber;
}
public void setCardNumber(Integer cardNumber) {
	this.cardNumber = cardNumber;
}
public String getModeOfPayment() {
	return modeOfPayment;
}
public void setModeOfPayment(String modeOfPayment) {
	this.modeOfPayment = modeOfPayment;
}
}
