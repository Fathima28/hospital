package dao;




import java.util.List;

import model.Admin;
import model.Patient;
import model.PatientDiagnosisDetails;
import model.Physician;


public interface HospitalDao {
	public boolean adminLogin(Admin admin);
   public void enroll(Patient patient); 
   public void addPhysician(Physician physician);
   public List<Physician> search(Physician physician);
   public void diagnosisDetails(PatientDiagnosisDetails diagnosis);
   public List<Patient> viewPatientHistory();
   
   
}
