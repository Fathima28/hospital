package dao;


import java.util.ArrayList;
import java.util.List;


import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import model.Admin;
import model.Patient;
import model.PatientDiagnosisDetails;
import model.Physician;


public class HospitalDaoImpl implements HospitalDao {
	@SuppressWarnings("rawtypes")
	public boolean adminLogin(Admin admin) {
		boolean isValid = false;
		Session session = new Configuration().configure("cfg.xml").buildSessionFactory().getCurrentSession();
		session.beginTransaction();
		Query q = session.createQuery("from Admin where uname=?1 and password=?2");
		q.setParameter(1, admin.getUname());
		q.setParameter(2, admin.getPassword());
		List list = q.getResultList();
		if (list != null && list.size() > 0) {
			isValid = true;
		}
		return isValid;
	}

	@Override
	public void enroll(Patient patient) {
		Session session = new Configuration().configure("cfg.xml").buildSessionFactory().getCurrentSession();
		Transaction tx = session.beginTransaction();
		session.save(patient);
		tx.commit();
	}

	@Override
	public void addPhysician(Physician physician) {
		Session session = new Configuration().configure("cfg.xml").buildSessionFactory().getCurrentSession();
		Transaction tx = session.beginTransaction();
		session.save(physician);
		tx.commit();

	}
	

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Physician> search(Physician physician) {
		Session session = new Configuration().configure("cfg.xml").buildSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<Physician> list=new ArrayList<Physician>();
		Query query = session.createQuery(
				"from Physician  where state=:state and insurancePlan=:insurancePlan and department=:department");
		query.setParameter("state", physician.getState());
		query.setParameter("insurancePlan", physician.getInsurancePlan());
		query.setParameter("department", physician.getDepartment());
		List<Physician> li =  query.list();
		for(Physician phy:li)
		{
			list.add(phy);
		}
		return list;
	}

	@Override
	public void diagnosisDetails(PatientDiagnosisDetails diagnosis) {
		Session session = new Configuration().configure("cfg.xml").buildSessionFactory().getCurrentSession();
		Transaction tx = session.beginTransaction();
		session.save(diagnosis);
		tx.commit();
	}


	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List<Patient> viewPatientHistory() {
		Session session=new Configuration().configure("cfg.xml").buildSessionFactory().openSession();
		session.beginTransaction();
		List<Patient> list= new ArrayList<Patient>();	
		Query q=session.createQuery("from EnrollPatient");
		List<Patient> li= q.list();
		for(Patient s:li) {
			list.add(s);
		}
		return list;
	}
	
}


